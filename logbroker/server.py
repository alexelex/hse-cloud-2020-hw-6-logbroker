import asyncio
import csv
import json
import os
import ssl
import datetime
import shutil
from io import StringIO
import uuid

from aiohttp.client import ClientSession
from aiohttp.client_exceptions import ClientError
from fastapi import FastAPI, Request, Response

from logger import get_module_logger

CH_HOST = os.getenv('LOGBROKER_CH_HOST', 'localhost')
CH_USER = os.getenv('LOGBROKER_CH_USER')
CH_PASSWORD = os.getenv('LOGBROKER_CH_PASSWORD')
CH_PORT = int(os.getenv('LOGBROKER_CH_PORT', 8123))
CH_CERT_PATH = os.getenv('LOGBROKER_CH_CERT_PATH')

LOGS_DIR = 'data'
STATUS_FILE = 'status'

TS_LEN = 10
UUID_LEN = 32

OK_STATUS = 'ok'
WAIT_STATUS = 'waiting'
FAIL_STATUS = 'failed'

logger = get_module_logger(__name__)


async def execute_query(query, data=None):
    url = f'http://{CH_HOST}:{CH_PORT}/'
    params = {
        'query': query.strip()
    }
    headers = {}
    if CH_USER is not None:
        headers['X-ClickHouse-User'] = CH_USER
        if CH_PASSWORD is not None:
            headers['X-ClickHouse-Key'] = CH_PASSWORD
    ssl_context = ssl.create_default_context(cafile=CH_CERT_PATH) if CH_CERT_PATH is not None else None

    async with ClientSession() as session:
        async with session.post(url, params=params, data=data, headers=headers, ssl=ssl_context) as resp:
            await resp.read()
            try:
                resp.raise_for_status()
                return resp, None
            except ClientError as e:
                return resp, {'error': str(e)}


app = FastAPI()


async def query_wrapper(query, data=None):
    res, err = await execute_query(query, data)
    if err is not None:
        logger.error(err)
        return err
    return await res.text()


@app.get('/show_create_table')
async def show_create_table(table_name: str):
    resp = await query_wrapper(f'SHOW CREATE TABLE "{table_name}";')
    if isinstance(resp, str):
        return Response(content=resp.replace('\\n', '\n'), media_type='text/plain; charset=utf-8')
    return resp


async def send_data(table_name, data, fmt):
    if fmt == 'json':
        fmt = 'JSONEachRow'
    elif fmt == 'list':
        fmt = 'CSV'
    elif fmt == 'binary':
        fmt = 'RowBinary'
    logger.info(data)
    logger.info(type(data))
    resp, err = await execute_query(f'INSERT INTO {table_name} FORMAT {fmt}', data)
    if err is not None:
        logger.error(err)
        return None, err
    return await resp.text(), err


def get_filename(table_name, uid, ts, fmt):
    return '{}_{}_{}_{}'.format(table_name, uid, ts, fmt)


def get_filepath(filename):
    return '{}/{}'.format(LOGS_DIR, filename)


def get_format(filename):
    if filename.endswith('json'):
        return 'json'
    if filename.endswith('list'):
        return 'list'
    if filename.endswith('binary'):
        return 'binary'


def get_file_info(filename):
    fmt = get_format(filename)
    filename = filename[:-(len(fmt) + 1)]
    ts = filename[-TS_LEN:]
    filename = filename[:-TS_LEN-1]
    uid = filename[-UUID_LEN:]
    table_name = filename[:-UUID_LEN-1]
    return table_name, uid, ts, fmt


def write_by_format(data, rows, fmt):
    if fmt == 'json':
        for row in rows:
            assert isinstance(row, dict)
            data.write(json.dumps(row))
            data.write('\n')
    elif fmt == 'list':
        cwr = csv.writer(data, quoting=csv.QUOTE_ALL)
        cwr.writerows(rows)


def load_binary_log(data, filepath):
    with open(filepath, 'wb+') as f:
        f.write(data)


async def load_on_disk(table_name, rows, format):
    ts = int(datetime.datetime.now().timestamp())
    uid = uuid.uuid4().hex
    key = get_filename(table_name, uid, ts, format)

    if format == 'binary':
        load_binary_log(rows, get_filepath(key))
        return key

    mode = 'a' if os.path.exists(get_filepath(key)) else 'w+'
    with open(get_filepath(key), mode) as f:
        data = StringIO()
        write_by_format(data, rows, format)
        data.seek(0)
        shutil.copyfileobj(data, f)
    return key


@app.post('/write_log')
async def write_log(request: Request):
    body = await request.json()
    res = []
    for log_entry in body:
        rows = log_entry['rows']
        if log_entry.get('format') in ('list', 'json') and \
                log_entry.get('table_name') is not None and \
                log_entry.get('rows') is not None:
            res.append(await load_on_disk(**log_entry))
        else:
            res.append({'error': f'unknown format {log_entry.get("format")}, you must use list or json'})
    return res


@app.post('/write_binary_log/{table_name}')
async def write_binary_log(table_name: str, request: Request):
    body = await request.body()
    return await load_on_disk(table_name, body, 'binary')


# table_name _ uuid _ ts _ format
@app.get('/status')
async def status(key: str):
    if not os.path.exists(STATUS_FILE):
        logger.warn('NO SUCH FILE')
        return WAIT_STATUS
    with open(STATUS_FILE, 'r') as f:
        for line in f.read().splitlines():
            state = get_status(line, key)
            if state is not None:
                return state
    return WAIT_STATUS


@app.get('/example')
async def example(file: str):
    process = await asyncio.subprocess.create_subprocess_shell(
        f'python3 client.py --url http://logbroker:8000 --csv {file}.csv --binary {file}.rowbinary', stdout=asyncio.subprocess.PIPE)

    logger.info(process.__str__())

    stdout, stderr = await process.communicate()
    logger.warn('RETURN CODE {}'.format(process.returncode))
    if stderr:
        return f'[stderr]\n{stderr.decode()}'
    if stdout:
        for line in stdout.decode().splitlines():
            logger.info(line)
        resp = f'[stdout]\n{stdout.decode()}'
        if isinstance(resp, str):
            return Response(content=resp.replace('\\n', '\n'), media_type='text/plain; charset=utf-8')
        return resp


def status_text(err, filename: str):
    if err is None:
        return ' '.join([filename, OK_STATUS])
    return ' '.join([filename, json.dumps(err), FAIL_STATUS])


def get_status(line: str, filename: str):
    line = line.split(' ', 1)
    if len(line) < 2 or line[0] != filename:
        return None
    if line[1].endswith(OK_STATUS):
        return OK_STATUS
    elif line[1].endswith(FAIL_STATUS):
        return json.loads(line[1][:-(len(FAIL_STATUS) + 1)])
    return {'error': 'unknown error'}


def get_order(files):
    ts_now = int(datetime.datetime.now().timestamp())
    order = dict()
    for file in files:
        table_name, _, ts, fmt = get_file_info(file)
        if ts_now <= int(ts):
            continue
        name = f'{table_name} {fmt}'

        if name not in order:
            order[name] = [file]
        else:
            order[name].append(file)
    return order


def unite_data(name, files, dirname):
    data = []
    for file in files:
        with open(os.path.join(dirname, file), 'r') as f:
            data.append(f.read())
    logger.info('\n'.join(data))
    return ''.join(data)


def load_data(name, files, dirname, fmt):
    logger.info('load_data')
    data = StringIO()
    rows = unite_data(name, files, dirname)
    data.write(rows)
    data.seek(0)
    return data.getvalue()


async def log_sender(dirname: str):
    while True:
        logger.info('log_sender loop')
        ts_now = int(datetime.datetime.now().timestamp())
        files = [f for f in os.listdir(dirname) if os.path.isfile(os.path.join(dirname, f))]

        block_files = []
        for file in files:
            table_name, _, ts, fmt = get_file_info(file)
            if ts_now <= int(ts):
                continue
            if fmt == 'binary':
                filepath = os.path.join(dirname, file)
                resp, err = await send_data(table_name, open(filepath, 'rb').read(), fmt)
                with open(STATUS_FILE, 'a') as f:
                    print(status_text(err, file), file=f, flush=True)
                os.remove(filepath)
            else:
                block_files.append(file)

        for pack, files_name in get_order(block_files).items():
            table_name, fmt = pack.split(' ', 1)
            resp, err = await send_data(table_name, load_data(pack, files_name, dirname, fmt), fmt)
            with open(STATUS_FILE, 'a') as f:
                for file in files_name:
                    print(status_text(err, file), file=f, flush=True)
                    os.remove(os.path.join(dirname, file))
        await asyncio.sleep(3)


@app.on_event("startup")
def on_startup():
    asyncio.create_task(log_sender(LOGS_DIR))


@app.get('/healthcheck')
async def healthcheck():
    return Response(content='Ok', media_type='text/plain')
