import argparse
import asyncio
from csv import reader
import json
import os
import pathlib
import typing as tp
from collections import namedtuple
from enum import Enum
import aiohttp
import urllib

Config = namedtuple('Config', ['session', 'URL'])


class Status(Enum):
    OK = 1
    FAIL = 2
    RUN = 3
    UNKNOWN = 4

    @staticmethod
    def from_str(s: str) -> 'Status':
        if s == '"ok"':
            return Status.OK
        elif s == '"failed"':
            return Status.FAIL
        elif s == '"waiting"':
            return Status.RUN
        return Status.UNKNOWN


def make_config(base_url: str) -> Config:
    return Config('', base_url)
    # return Config(aiohttp.ClientSession(json_serialize=ujson.dumps), base_url)


def build_url(url: str, path: str, args_dict: tp.Dict[str, tp.Any]):
    url_parts = list(urllib.parse.urlparse(url))
    url_parts[2] = path
    url_parts[4] = urllib.parse.urlencode(args_dict)
    return urllib.parse.urlunparse(url_parts)


def make_log_entry(table_name: str, rows: tp.Any, fmt: str) -> tp.Dict[str, tp.Any]:
    return {
        "table_name": table_name,
        "rows": rows,
        "format": fmt
    }


async def write_log(log_entries: tp.List[tp.Dict[str, tp.Any]], conf: Config) -> tp.Optional[tp.List[str]]:
    print(f"curl post {urllib.parse.urljoin(conf.URL, 'write_log')} -d {json.dumps(log_entries).encode('ascii')}")
    async with aiohttp.ClientSession() as session:
        async with session.post(urllib.parse.urljoin(conf.URL, 'write_log'),
                                data=json.dumps(log_entries).encode('ascii')) as resp:
            data = await resp.read()
            if not resp.ok:
                print(await resp.text())
                return None
            return json.loads(data)


async def write_binary_log(table_name: str, filename: str, conf: Config) -> tp.Optional[str]:
    print(f"curl post {urllib.parse.urljoin(conf.URL, f'write_binary_log/{table_name}')}")
    async with aiohttp.ClientSession() as session:
        with open(filename, 'rb') as f:
            async with session.post(urllib.parse.urljoin(conf.URL, f'write_binary_log/{table_name}'),
                                    data=f.read()) as resp:
                data = await resp.read()
                if not resp.ok:
                    print(await resp.text())
                    return None
                return data.decode('ascii')[1:-1]


async def get_state(key: str, conf: Config) -> Status:
    print(f"curl get {build_url(conf.URL, 'status', {'key': key})}")
    async with aiohttp.ClientSession() as session:
        async with session.get(build_url(conf.URL, 'status', {'key': key})) as resp:
            data = await resp.read()
            if not resp.ok:
                return Status.UNKNOWN
            return Status.from_str(data.decode('ascii').strip())


def read_csv_log_entry(table_name: str, filename: str, formats: tp.Optional[tp.List[type]] = None) -> tp.Dict[str, tp.Any]:
    with open(filename, 'r') as f:
        csv_reader = reader(f)
        list_of_rows = list(csv_reader)
        if formats:
            list_of_rows = [list(map(line, formats)) for line in list(csv_reader)]
        return make_log_entry(table_name, list_of_rows, 'list')


async def example(url: str, list_file: str, binary_file: str, table: str):
    logs = [
        make_log_entry(table, [{'a': 1, 'b': '3'}], 'json'),
        make_log_entry(table, [{'a': 7, 'b': '9'}], 'json'),
        make_log_entry(table, [[2, '4'], [3, '5']], 'list'),
        read_csv_log_entry(table, list_file, [int, str]),
    ]

    conf = make_config(url)

    keys = await write_log(logs, conf)

    keys.append(await write_binary_log(table, binary_file, conf))

    if keys is None:
        print('keys is None')
        return

    for i in range(3):
        for key in keys:
            status = await get_state(key, conf)
            print(status, flush=True)
        await asyncio.sleep(3)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--url', type=str, required=True)
    parser.add_argument('--csv', type=str, required=True)
    parser.add_argument('--binary', type=str, required=True)
    parser.add_argument('--table_name', type=str, default='kek')

    args = parser.parse_args()

    if not os.path.exists(args.csv) or not os.path.isfile(args.csv):
        print('no such file: ', args.csv)
        exit(2)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(example(args.url, args.csv, args.binary, args.table_name))
